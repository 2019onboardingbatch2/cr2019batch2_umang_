//package tables
//
//import slick.driver.H2Driver.api._
//import tables.Main.Products
//
//case class Orders(orderId: Long, totalAmount: Long, orderedDate: String)
//
//
//class OrdersTable(tag: Tag) extends Table[Orders](tag, "orders") {
//  def orderId = column[String]("orderId")
//
//  def amount = column[Long]("amount")
//
//  def createdAt = column[String]("createdAt")
//
//  def * = (orderId, amount, createdAt) <> (Orders.tupled, Orders.unapply)
//}
