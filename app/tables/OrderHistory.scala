//package tables
//
//import slick.driver.H2Driver.api._
//import tables.Main.Products
//
//trait OrderStatus{
//  def status : String
//}
//
//object OrderStatus {
//  implicit val dbMapping = MappedColumnType.base[OrderStatus, String](
//    _.status,
//    _ => order_confirmed: OrderStatus
//  )
//}
//
//case object order_confirmed extends OrderStatus {
//  override val status = "ORDER_CONFIRMED"
//}
//
//case class OrderHistory(historyId: Long, orderId: String, status: OrderStatus, createdAt: String, updatedAt: String, superseadedBy: Option[Long])
//
//
//class OrderHistoryTable(tag: Tag) extends Table[OrderHistory](tag, "orders") {
//  def historyId = column[Long]("historyId")
//  def orderId = column[Long]("amount")
//  def status = column[OrderStatus]("status")
//  def createdAt = column[String]("createdAt")
//  def updatedAt = column[String]("updatedAt")
//  def superSeadedBy = column[Option[Long]]("superSeadedBy")
//
//  def * = (historyId, orderId, status,createdAt,updatedAt,superSeadedBy) <> (Orders.tupled, Orders.unapply)
//}
