//package tables
///*
//Try to create two TableDefinition classes Products and Orders
//Products Table will have columns: productId, productName, productPricePerUnit
//Order table will have orderId, totalAmount and orderedDate
// */
//
//import slick.driver.H2Driver.api._
//
//import scala.concurrent._
//import scala.concurrent.duration._
//
//
///*Try to create two Table Definition classes Products and Orders
//
//Products Table will have columns: productId, productName, productPricePerUnit
//
//Order table will have orderId, totalAmount and orderedDate*/
//
//object Main {
//
//  case class Products(productId: Long = 0L, productName: String, productPricePerUnit: Int)
//
//  case class Orders(orderId: Long, totalAmount: Long, orderedDate: String)
//
//  class ProductTable(tag: Tag) extends Table[Products](tag,  m,"product") {
//    def productId = column[Long]("productId")
//
//    def productName = column[String]("productName")
//
//    def productPricePerUnit = column[Int]("productPricePerUnit")
//
//    def * = (productId, productName, productPricePerUnit) <> (Products.tupled, Products.unapply)
//  }
//
//
//  class OrderTable(tag: Tag) extends Table[Orders](tag, "order") {
//    def orderId = column[Long]("orderId")
//
//    def totalAmount = column[Long]("totalAmount")
//
//    def orderedDate = column[String]("orderedDate")
//
//    def * = (orderId, totalAmount, orderedDate) <> (Orders.tupled, Orders.unapply)
//
//  }
//
//  lazy val ProductTable = TableQuery[ProductTable]
//  lazy val OrderTable = TableQuery[OrderTable]
//
//  val createTableActionForProduct = ProductTable.schema.create // ProductTable is lazy val not class
//  val createTableActionForOrder = OrderTable.schema.create
//
//  val insertProductAction =
//    ProductTable ++= Seq(Products(1, "p2", 90), Products(2, "p1", 90), Products(3, "p3", 90), Products(4, "p4", 95))
//  val insertOrderAction = OrderTable ++= Seq(Orders(1, 2, "sd"), Orders(1, 2, "df"))
//
//  val selectProductAction = ProductTable.result
//  val selectOrderAction = OrderTable.result
//
//
//  val db = Database.forConfig("scalaxdb")
//
//
//  def exec[T](action: DBIO[T]): T =
//    Await.result(db.run(action), 2 seconds)
//
//  db.run()
//
//
//  def main(args: Array[String]): Unit = {
//    exec(createTableActionForProduct)
//    exec(createTableActionForOrder)
//    exec(insertOrderAction)
//    exec(insertProductAction)
//    exec(selectProductAction).foreach(println)
//  }
//}