package models

import play.api.libs.json.{Json, OFormat}
import slick.driver.PostgresDriver.api._

case class Productt(productId: ProductId,
                    productName: String,
                    sellerId: SellerId,
                    price: Long)


object Productt {
  implicit val productsFormat: OFormat[Productt] = Json.format[Productt]
}
class ProductsTable (tag: Tag) extends Table[Productt] (tag, "Products") {

  def productId = column[ProductId]("productId",O.PrimaryKey, O.AutoInc)
  def productName = column[String]("productName")
  def sellerId = column[SellerId]("sellerId")
  def price = column[Long]("price")

  override def * = (productId, productName, sellerId, price).
    <>((Productt.apply _).tupled, Productt.unapply)
}