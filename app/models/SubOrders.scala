package models


import play.api.libs.json.{JsResult, JsString, JsValue, Json, OFormat, Reads, Writes}
import slick.driver.PostgresDriver.api._
import services.SlickOrdersService.orderTableQuery


case class SubOrderId ( id: Long )
object SubOrderId {
  implicit val subOrderIdColumnType: BaseColumnType[SubOrderId] =
    MappedColumnType.base[SubOrderId, Long](subOrderId => subOrderId.id,
      subOrderIdLong => SubOrderId(subOrderIdLong))


  implicit val subOrderIdReads: Reads[SubOrderId] = new Reads[SubOrderId] {
    override def reads(json: JsValue): JsResult[SubOrderId] = json.validate[Long].map(SubOrderId(_))
  }
  implicit  val subOrderIdWrites: Writes[SubOrderId] = new Writes[SubOrderId] {
    override def writes(o: SubOrderId): JsValue = Json.toJson(o.id)
  }

}

case class SellerId ( id: Long )
object SellerId {
  implicit val sellerIdColumnType: BaseColumnType[SellerId] =
    MappedColumnType.base[SellerId, Long]( sellerId => sellerId.id,
      sellerIdLong => SellerId(sellerIdLong))


  implicit val sellerIdReads: Reads[SellerId] = new Reads[SellerId] {
    override def reads(json: JsValue): JsResult[SellerId] = json.validate[Long].map(SellerId(_))
  }
  implicit  val sellerIdWrites: Writes[SellerId] = new Writes[SellerId] {
    override def writes(o: SellerId): JsValue = Json.toJson(o.id)
  }

}

sealed trait SubOrderStatus{
  def subOrderStatus : String
}

object SubOrderStatus {

  case object Accepted extends SubOrderStatus {
    override val subOrderStatus: String = "Accepted"
  }
  case object Rejected extends SubOrderStatus {
    override val subOrderStatus: String = "Rejected"
  }

  val subOrderStatusSet: Set[SubOrderStatus] = Set[SubOrderStatus](Accepted, Rejected)

  def toSuborderStatus( subOrderStatusStr: String): SubOrderStatus = subOrderStatusSet.collectFirst{
    case subOrderStatus if subOrderStatus.subOrderStatus == subOrderStatusStr => subOrderStatus
  }.getOrElse{
    throw new Exception("Incorrect SubOrderStatus Type String Provided")
  }

  implicit val subOrderStatusColumnType: BaseColumnType[SubOrderStatus] =
    MappedColumnType.base[SubOrderStatus, String](subOrderStatus => subOrderStatus.subOrderStatus,
      toSuborderStatus)

  implicit val subOrderStatusReads: Reads[SubOrderStatus] =  new Reads[SubOrderStatus] {
    override def reads(json: JsValue): JsResult[SubOrderStatus] = json.validate[String].map(toSuborderStatus)
  }

  implicit val subOrderStatusWrites: Writes[SubOrderStatus] = new Writes[SubOrderStatus] {
    override def writes(o: SubOrderStatus): JsValue = JsString(o.subOrderStatus)
  }
}



case class SubOrders( subOrderId: SubOrderId,
                      orderId: OrderId,
                      sellerId: SellerId,
                      subOrderStatus: SubOrderStatus)

object SubOrders {
  implicit val subOrderFormat: OFormat[SubOrders] = Json.format[SubOrders]
}
class SubOrderTable(tag: Tag) extends Table[SubOrders] ( tag, "SubOrders" ) {

  def subOrderId = column[SubOrderId]("subOrderId", O.PrimaryKey, O.AutoInc)
  def orderId = column[OrderId]("orderId")
  def sellerId = column[SellerId]("sellerId")
  def subOrderStatus = column[SubOrderStatus]("subOrderStatus")
  def orderId_FK = foreignKey("orderId_FK",orderId,orderTableQuery)(_.id)


  override def * = (subOrderId, orderId, sellerId, subOrderStatus)
    .<>((SubOrders.apply _).tupled, SubOrders.unapply)
}

