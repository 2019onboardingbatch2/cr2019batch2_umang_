package models

import play.api.libs.json.{JsResult, JsValue, Json, OFormat, Reads, Writes}
import slick.driver.PostgresDriver.api._

case class OrderItemId ( orderItemId: Long )
object OrderItemId {

  implicit val orderItemIdReads: Reads[OrderItemId] = new Reads[OrderItemId] {
    override def reads(json: JsValue): JsResult[OrderItemId] =
      json.validate[Long].map(OrderItemId(_))
  }

  implicit val orderItemIdWrites: Writes[OrderItemId] = new Writes[OrderItemId] {
    override def writes(o: OrderItemId): JsValue = Json.toJson(o.orderItemId)
  }


  implicit val orderItemIdColumnType: BaseColumnType[OrderItemId] =
    MappedColumnType.base[OrderItemId, Long](orderItemId => orderItemId.orderItemId,
      orderItemIdLong => OrderItemId(orderItemIdLong))
}

case class ProductId ( productId: Long )
object ProductId {

  implicit val productIdReads: Reads[ProductId] = new Reads[ProductId] {
    override def reads(json: JsValue): JsResult[ProductId] = json.validate[Long].map(ProductId(_))
  }
  implicit val productIdWrites: Writes[ProductId] = new Writes[ProductId] {
    override def writes(o: ProductId): JsValue = Json.toJson(o.productId)
  }

  implicit val productIdColumnType: BaseColumnType[ProductId] =
    MappedColumnType.base[ProductId, Long]( productId => productId.productId,
      productIdLong => ProductId(productIdLong))
}

case class OrderItems ( orderItemId: OrderItemId,
                        productId: ProductId,
                        subOrderId: SubOrderId,
                        productName: String,
                        quantity: Int,
                        price: BigDecimal)

object OrderItems {
  implicit val orderItemFormat: OFormat[OrderItems] = Json.format[OrderItems]
}

class OrderItemTable (tag: Tag) extends Table[OrderItems] (tag, "OrderItems") {

  def orderItemId = column[OrderItemId]("orderItemId",O.PrimaryKey, O.AutoInc)
  def productId = column[ProductId]("productId")
  def subOrderId = column[SubOrderId]("subOrderId")
  def productName = column[String]("productName")
  def quantity = column[Int]("quantity")
  def price = column[BigDecimal]("price")



  override def * = (orderItemId, productId, subOrderId, productName, quantity, price)
    .<>((OrderItems.apply _).tupled, OrderItems.unapply)
}

