package models

/*
Id: OrderId, (base type : Long)
userId: UserId, (base type : Long)
userAddressId: Option[UserAddressId] (base type : Long)
status:OrderStatus (OrderStatus -> Success, Failed) (baseType: String)
paymentInstrument: PaymentInstrument (PaymentInstrument -> CreditCard, NetBanking) (baseType: String)
orderedAt: DateTime (base type: java.sql.TimeStamp)
*/

import java.sql.Timestamp

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import play.api.libs.json._
import slick.driver.PostgresDriver.api._

case class OrderId(orderId: Long)
object OrderId {

  implicit val orderIdReads: Reads[OrderId] = new Reads[OrderId] {
    override def reads(json: JsValue): JsResult[OrderId] =
      json.validate[Long].map(OrderId(_))
  }

  implicit val orderIdWrites: Writes[OrderId] = new Writes[OrderId] {
    override def writes(o: OrderId): JsValue = Json.toJson(o.orderId)
  }

  implicit val orderIdColumnType: BaseColumnType[OrderId] =
    MappedColumnType.base[OrderId, Long](orderId => orderId.orderId, id => OrderId(id))
}
case class UserId(userId: Long)
object UserId {

  implicit val userIdReads: Reads[UserId] = new Reads[UserId] {
    override def reads(json: JsValue): JsResult[UserId] =
      json.validate[Long].map(UserId(_))
  }

  implicit val userIdWrites: Writes[UserId] = new Writes[UserId] {
    override def writes(o: UserId): JsValue = Json.toJson(o.userId)
  }

  implicit val userIdColumnType: BaseColumnType[UserId] =
    MappedColumnType.base[UserId, Long](userId => userId.userId, id => UserId(id))
}
case class UserAddressId(userAddressId: Long)
object UserAddressId {

  implicit val userAddressIdReads: Reads[UserAddressId] = new Reads[UserAddressId] {
    override def reads(json: JsValue): JsResult[UserAddressId] =
      json.validate[Long].map(UserAddressId(_))
  }

  implicit val userAddressIdWrites: Writes[UserAddressId] = new Writes[UserAddressId] {
    override def writes(o: UserAddressId): JsValue = Json.toJson(o.userAddressId)
  }

  implicit val userAddressIdColumnType: BaseColumnType[UserAddressId] =
    MappedColumnType.base[UserAddressId, Long](userAddressId => userAddressId.userAddressId,
      addressId => UserAddressId(addressId))
}

trait OrderStatus{
  def status: String
}
object OrderStatus{
  case object Success extends OrderStatus {
    override val status: String = "Success"
  }
  case object Failure extends OrderStatus {
    override val status: String = "Failure"
  }
  val orderStatusSet: Set[OrderStatus] = Set[OrderStatus](Success, Failure)

  def toOrderStatus(orderStatusString: String): OrderStatus = orderStatusSet.collectFirst{
    case status if status.status == orderStatusString => status
  }.getOrElse{
    throw new Exception("Order Status not found for the given String")
  }

  implicit val orderStatusReads: Reads[OrderStatus] = new Reads[OrderStatus] {
    override def reads(json: JsValue): JsResult[OrderStatus] = json.validate[String].map(toOrderStatus)
  }

  implicit val orderStatusWrites: Writes[OrderStatus] = new Writes[OrderStatus] {
    override def writes(o: OrderStatus): JsValue = JsString(o.status)
  }

  implicit val orderStatusColumnType: BaseColumnType[OrderStatus] =
    MappedColumnType.base[OrderStatus, String](orderStatus => orderStatus.status, toOrderStatus)
}

sealed trait PaymentInstrument{
  def paymentType: String
}

object PaymentInstrument {
  case object CreditCard extends PaymentInstrument {
    override def paymentType: String = "CreditCard"
  }
  case object NetBanking extends PaymentInstrument {
    override def paymentType: String = "NetBanking"
  }

  val paymentTypeSet: Set[PaymentInstrument] = Set(CreditCard, NetBanking)

  def toPaymentInstrumentType(paymentInstrumentString: String): PaymentInstrument = paymentTypeSet.collectFirst{
    case paymentInstrumentType if paymentInstrumentType.paymentType == paymentInstrumentString => paymentInstrumentType
  }.getOrElse{
    throw new Exception("Incorrect Payment Instrument Type String Provided")
  }

  implicit val paymentInstrumentReads: Reads[PaymentInstrument] = new Reads[PaymentInstrument] {
    override def reads(json: JsValue): JsResult[PaymentInstrument] =
      json.validate[String].map(toPaymentInstrumentType)
  }

  implicit val paymentInstrumentWrites: Writes[PaymentInstrument] = new Writes[PaymentInstrument] {
    override def writes(o: PaymentInstrument): JsValue = JsString(o.paymentType)
  }

  implicit val paymentInstrumentColumnType: BaseColumnType[PaymentInstrument] =
    MappedColumnType.base[PaymentInstrument,String](
      PaymentInstrument => PaymentInstrument.paymentType,toPaymentInstrumentType )
}





case class Order(Id: OrderId, userId: UserId, userAddressId: Option[UserAddressId],
                  status: OrderStatus, paymentInstrument: PaymentInstrument, orderedAt: DateTime)

object Order {
  //  implicit val dateTimeReads: Reads[DateTime] = play.api.libs.json.Reads.jodaDateReads("yyyy-MM-dd")

  val dateTimeFormatter: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  implicit val dateTimeReads: Reads[DateTime] = new Reads[DateTime] {
    override def reads(json: JsValue): JsResult[DateTime] = json.validate[String].map{
      json => dateTimeFormatter.parseDateTime(json)
    }
  }

  implicit val dateTimeWrites: Writes[DateTime] = new Writes[DateTime] {
    override def writes(o: DateTime): JsValue = JsString(dateTimeFormatter.print(o))
  }

  implicit val dateTimeColumnType: BaseColumnType[DateTime] =
    MappedColumnType.base[DateTime,String](dt => dt.toString,
      dtStr => DateTime.parse(dtStr, dateTimeFormatter))


  implicit val ordersFormat: OFormat[Order] = Json.format[Order]

}

class OrdersTable(tag: Tag) extends Table[Order](tag, "orders") {

  implicit val dateColumnType: BaseColumnType[DateTime] =
    MappedColumnType.base[ DateTime, Timestamp ]( dateTime => new Timestamp( dateTime.getMillis ),
      timestamp => new DateTime( timestamp.getTime ) )

  def id = column[OrderId]("id",O.PrimaryKey,O.AutoInc)
  def userId = column[UserId]("userId")
  def userAddressId = column[Option[UserAddressId]]("userAddressId")
  def status = column[OrderStatus]("status")
  def paymentInstrument = column[PaymentInstrument]("paymentType")
  def createdAt = column[DateTime]("createdAt")

  def * = (id, userId, userAddressId, status, paymentInstrument, createdAt) <> ((Order.apply _).tupled, Order.unapply)

}



