package services

import javax.inject.Singleton

import scala.collection.mutable

@Singleton
class StoreWeatherInfoServices {
  val citySet = mutable.Set[String]()
  val cityWeatherData = mutable.Set[String]()
  def storeWeatherData(stringWeatherDate: String): Unit ={
    citySet.add(stringWeatherDate)
    cityWeatherData.add(stringWeatherDate)
    println(citySet)
  }
}