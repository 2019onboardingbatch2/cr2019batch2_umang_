package services

import javax.inject.{Inject, Singleton}
import play.api.libs.ws.WSClient

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class FetchWeatherInfoService @Inject ()(WSClient: WSClient ) {
  def getWeatherData(url: String)(implicit ec: ExecutionContext): Future[String] = {
    val weatherInfoStr = WSClient.url(url).get().map(_.body)
    weatherInfoStr
  }
}
//    for{
//      stringData: String <- weatherInfoStr
//    } yield {
//      //storeWeatherServices.storeWeatherData(stringData)
//    }
//  }

