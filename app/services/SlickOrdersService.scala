package services

import com.google.inject.Singleton
import models.SubOrderStatus.Accepted
import org.joda.time.DateTime
import play.api.libs.json.{Json, OFormat}
import services.SlickOrdersService.{OrderInputDTO, OrderItemsDTO, SubOrderDTO}
import slick.driver.PostgresDriver.api._
import models._
import models.OrderStatus.Success

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class SlickOrdersService {

  implicit val listOfTupleFormat: OFormat[(Order, SubOrders, OrderItems)] =
    Json.format[(Order, SubOrders, OrderItems)]

  lazy val orderTableQuery = TableQuery[OrdersTable]
  lazy val subOrderTableQuery = TableQuery[SubOrderTable]
  lazy val orderItemTableQuery = TableQuery[OrderItemTable]
  lazy val productsTableQuery = TableQuery[ProductsTable]

  val db = Database.forConfig("postgresDb")
  def dbRun[T](action: DBIO[T]): Future[T] = {
    db.run(action)
  }

  def getAllProducts() :Future[Seq[Productt]] = {
    dbRun(productsTableQuery.result)
  }

  //getAllOrders
  def getAllOrders: Future[Seq[(Order, SubOrders, OrderItems)]] = {

    dbRun(
      (for {
        order <- orderTableQuery
        subOrder <- subOrderTableQuery if order.id === subOrder.orderId
        orderItems <- orderItemTableQuery if subOrder.subOrderId === orderItems.subOrderId
      } yield { (order, subOrder, orderItems)
      }).result
    )

  }

  def addOrderItems(orderItem: OrderItemsDTO, subOrderId: SubOrderId): DBIO[Unit] = {

    (for {
      _ <- orderItemTableQuery +=
        OrderItems(OrderItemId(-1),orderItem.productId, subOrderId, orderItem.productName,
          orderItem.quantity, orderItem.price)
    } yield {}).transactionally

  }

  def addSubOrders(orderId:OrderId, subOrderDTO: SubOrderDTO):DBIO[Unit] = {

    (for {
      subOrderId <- subOrderTableQuery returning subOrderTableQuery.map(_.subOrderId) +=
        SubOrders(SubOrderId(-1), orderId ,subOrderDTO.sellerId, Accepted )

      _ <-  DBIO.sequence(subOrderDTO.orderItemsList.map{ orderItem => addOrderItems(orderItem, subOrderId)})

    } yield {}).transactionally

  }
  //addProduct
//case class Product(productId: ProductId,
//                    productName: String,
//                    sellerId: SellerId,
//                    price: Long)
  def addProduct(ProductDTO: Productt): Future[Unit] = {
    dbRun(
      (
        for{
          //_ <- productsTableQuery.schema.create
          res <- productsTableQuery+=Productt(ProductId(-1), ProductDTO.productName, ProductDTO.sellerId, ProductDTO.price)
        } yield {
          res
        }
      )
    )
  }

  def addOrderDetails(orderInputDTO: OrderInputDTO): Future[Unit] = {
    val dateTime = DateTime.now()
    dbRun(
      (

        for {
//          _ <- orderItemTableQuery.schema.create
//          _ <- orderTableQuery.schema.create
//          _ <- subOrderTableQuery.schema.create
          orderId <- orderTableQuery returning orderTableQuery.map(_.id) +=
            Order(OrderId(-1), orderInputDTO.userId, Some(orderInputDTO.userAddressId),
              orderInputDTO.orderStatus, orderInputDTO.paymentInstrument, dateTime);

          _ <- DBIO.sequence(orderInputDTO.subOrdersList.map(subOrderDTO =>
            addSubOrders(orderId, subOrderDTO)))
        } yield {

        }
      )
    )

  }
  //////////////////////////////////////////////// Functions for second API ---------
  def getOrderForUser(orderId: OrderId, userId: UserId): Future[List[(Order, SubOrders, OrderItems)]] = {

    val   searchUserForOrder = orderTableQuery.filter( row => row.id === orderId && row.userId === userId)

    // How to do this sequentially ???????
    // If not handled querry will return an empty list and that can be handled accordingly
//    searchUserForOrder.result.map{x => x.headOption match {
//      case None => throw new Exception("UserId not same for the Searched OrderId")
//    }}

    dbRun(
      (for {
      ((order, subOrder),orderItem) <- searchUserForOrder
        .join(subOrderTableQuery).on{case (order, subOrder)=> order.id === subOrder.orderId}
        .join(orderItemTableQuery).on{case ((order, subOrder),orderItem) =>
        (order, subOrder)._2.subOrderId === orderItem.subOrderId}
    } yield {
      (order, subOrder, orderItem)
    }).result.map(_.toList)
    )
  }
  /////////////// Third API
  def getOrderItemListBySubOrderId(subOrderId: SubOrderId): DBIO[List[OrderItemsDTO]] = {
    for {
      orderItemList <- orderItemTableQuery.filter(_.subOrderId === subOrderId).result.map(_.toList).map { orderItems =>
        orderItems.map( y => OrderItemsDTO(y.productId, y.productName, y.quantity, y.price)) }
    } yield {
      orderItemList
    }
  }

  def getSubOrderListByOrderId(order: Order): DBIO[DBIO[Seq[SubOrderDTO]]] = {
    for  {
      subOrderSeq <- subOrderTableQuery.filter(_.orderId === order.Id).result
      suborderList = DBIO.sequence(subOrderSeq.map ( subOrder =>
        getOrderItemListBySubOrderId(subOrder.subOrderId).map( orderItemList =>
          SubOrderDTO(subOrder.sellerId,orderItemList) )))
    } yield {
      suborderList
    }
  }

  def getBulkOrderDTOs( orderIdList: List[OrderId] ): Future[List[OrderInputDTO]] = {

    // How to do with inset Bind
    dbRun(
      DBIO.sequence(orderIdList.map{orderId =>
        for {
          order <- orderTableQuery.filter(_.id === orderId).result.headOption.map(_.getOrElse(throw new Exception))

          subOrderListDBIO <- getSubOrderListByOrderId(order)

          orderDto <- subOrderListDBIO.map( subOrderList =>
            OrderInputDTO(order.userId,order.userAddressId.getOrElse(throw new Exception),
              order.status, order.paymentInstrument, subOrderList.toList ))

        } yield {
          orderDto
        }
      })
    )
  }

  def getOrderDTO(orderId: OrderId): DBIO[OrderInputDTO] = {
    for {
      order <- orderTableQuery.filter(_.id === orderId).result.headOption.map(_.getOrElse(throw new Exception))

      subOrderListDBIO <- getSubOrderListByOrderId(order)

      orderDto <- subOrderListDBIO.map( subOrderList =>
        OrderInputDTO(order.userId,order.userAddressId.getOrElse(throw new Exception),
          order.status, order.paymentInstrument, subOrderList.toList ))

    } yield {
      orderDto
    }
  }

  def getOrderDTOsDBIO(orderIdList: List[OrderId]): DBIO[List[OrderInputDTO]] = {

    DBIO.sequence(orderIdList.map{orderId =>
      getOrderDTO(orderId)
    })
  }

  def getOrderDTOByOrderItemId(orderItemId: OrderItemId): Future[List[OrderInputDTO]] = {
    dbRun(
      (for {
        orderItem <- orderItemTableQuery.filter( _.orderItemId === orderItemId ).result.headOption

        subOrder <- subOrderTableQuery.filter (_.subOrderId ===
          orderItem.getOrElse(throw new Exception("Invalid Order Item Id")).subOrderId).result.headOption

        orders <- orderTableQuery.filter(_.id ===
          subOrder.getOrElse(throw new Exception("Invalid subOrderId Found")).orderId).result

        orderDto <- getOrderDTOsDBIO(orders.toList.map(_.Id))
      } yield {
        orderDto
      })
    )
  }




  def addOrderFromForm (userId: UserId, userAddressId: UserAddressId,
                                    paymentInstrument: PaymentInstrument,
                                    productIdList: Seq[ProductId]): Unit = {

    val productSeqFut = dbRun(productsTableQuery.filter(_.productId inSetBind productIdList).result)

    val orderItemsDTOListFut = for {
      productSeq <- productSeqFut
    } yield {
      productSeq.map { product => (product.sellerId,
        OrderItemsDTO( product.productId, product.productName, 1, product.price ))
      }
      }.toList

    val subOrderDTOListFut = orderItemsDTOListFut.map{
      sellerIdOrderItemDTOTupleList =>
        sellerIdOrderItemDTOTupleList.groupBy( sellerIdOrderItemDTOTuple =>
          sellerIdOrderItemDTOTuple._1 )
          .mapValues{ sellerIdOrderItemDTOList =>
            sellerIdOrderItemDTOList.map{ case (sellerId,orderItemsDTO) => orderItemsDTO}}
          .map{ case (sellerId, orderItemsDTOList) =>
            SubOrderDTO( sellerId, orderItemsDTOList)
          }.toList
    }

    subOrderDTOListFut.map{ z =>
      OrderInputDTO(userId,userAddressId, Success , paymentInstrument, z )
    }.map(addOrderDetails)

  }
  ////

  /*
  override def getAllOrdersLast15DaysDBIO(userId: UserId): DBIO[List[OrderInputDTO]] = {
    val time15DaysBefore = DateTime.now().minusDays(15)

    (for {
      orders <- orderTableQuery.filter( orderRow => orderRow.userId === userId && orderRow.createdAt >=  time15DaysBefore ).result
      orderDto <- getOrderDTOsDBIO(orders.map(_.Id).toList)
    } yield {
      orderDto
    })

  }


  def getAllOrdersLast15Days(userId: UserId): Future[List[OrderInputDTO]] = {
    dbRun(getAllOrdersLast15DaysDBIO(userId))
  }

   */
}

object SlickOrdersService {

  case class QueryOrderForUser(orderId: OrderId, userId: UserId)

  implicit val listOfTupleFormat: OFormat[(Order, SubOrders, OrderItems)] =
    Json.format[(Order, SubOrders, OrderItems)]

  case class OrderItemsDTO (  productId: ProductId,
                              productName: String,
                              quantity: Int,
                              price: BigDecimal )
  object OrderItemsDTO {
    implicit val orderItemFormat: OFormat[OrderItemsDTO] = Json.format[OrderItemsDTO]
  }

  case class SubOrderDTO( sellerId: SellerId,
                          orderItemsList: List[OrderItemsDTO])

  object SubOrderDTO {
    implicit val subOrderFormat: OFormat[SubOrderDTO] = Json.format[SubOrderDTO]
  }


  case class OrderInputDTO( userId: UserId,
                            userAddressId: UserAddressId,
                            orderStatus: OrderStatus,
                            paymentInstrument: PaymentInstrument,
                            subOrdersList: List[SubOrderDTO])

  object OrderInputDTO {
    implicit val orderInputDTOFormat: OFormat[OrderInputDTO] = Json.format[OrderInputDTO]
  }

  lazy val orderTableQuery = TableQuery[OrdersTable]
  lazy val subOrderTableQuery = TableQuery[SubOrderTable]
  lazy val orderItemTableQuery = TableQuery[OrderItemTable]
  lazy val productsTableQuery = TableQuery[ProductsTable]


  val db = Database.forConfig("postgresDb")

}
