package controllers

import javax.inject.{Inject, Singleton}
import models.{Order, OrderId, OrderItemId, OrderItems, Productt, SubOrders, UserId}
import play.api.Logger
import play.api.libs.json._
import play.api.mvc.{Action, AnyContent, Controller}
import services.SlickOrdersService
import services.SlickOrdersService.OrderInputDTO

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._


@Singleton
class SlickOrders @Inject()(slickOrders: SlickOrdersService)extends  Controller() {
  def addorder() = Action {
    Ok("Do Something")
  }



  def index() = Action {
    println("reaching here")
    Ok("Do Something")
  }


  def addProduct() = Action.async(parse.json) { implicit request =>
    println("reaching inside addProduct Method")

    request.body.validate[Productt] match {
      case JsSuccess(value, _) => slickOrders.addProduct(value).map{ _ =>
        Ok(Json.obj("status" -> "Success", "Message" -> "Product added successFully"))
      }
      case JsError(errors) =>
        Logger.error(s"ERROR VALIDATING Product DTO $errors")
        Future.successful(BadRequest(Json.obj("status" -> "FAILURE", "Error-Message" -> "Error in validating INPUT_DTO")))
    }
    }



  def addOrderDetails() = Action.async(parse.json) { implicit request =>
    println("reaching here")

    request.body.validate[OrderInputDTO] match {

      case JsSuccess(value, _) => slickOrders.addOrderDetails(value).map { _
      =>
        Ok(Json.obj("status" -> "SUCCESS", "Message" -> "Order and orderdetails inserted in DB successfully"))
      }.recover {
        case exception =>
          Logger.error(s"Error while Returning Future in InsertOrders :: $exception ")
          InternalServerError(Json.obj("status" -> "ERROR"))
      }
      case JsError(errors) =>
        Logger.error(s"ERROR VALIDATING ORDER_INPUT_DTO $errors")
        Future.successful(BadRequest(Json.obj("status" -> "FAILURE", "Error-Message" -> "Error in validating INPUT_DTO")))
    }
  }

  def getOrderForUser(orderId: Long, userId: Long): Action[AnyContent] = Action.async { implicit request =>

    // This implicit was not working when I put it in slickOrderService class !!! - ? - strange ?
    implicit val listOfTupleFormat: OFormat[(Order, SubOrders, OrderItems)] =
      Json.format[(Order, SubOrders, OrderItems)]

    slickOrders.getOrderForUser(OrderId(orderId), UserId(userId)).map(responseDTO =>
      Ok(Json.toJson(responseDTO))
    ).recover {
      case exception =>
        Logger.error(s"Error While Fetching order DTO from DB for orderId - $orderId , err - $exception")
        InternalServerError(Json.obj("status" -> "failure", "GET_ORDER_FOR_USER" -> "FAILED"))
    }

  }

  def getBulkOrderDTOs: Action[JsValue] = Action.async(parse.json) { implicit request =>

    request.body.validate[List[OrderId]] match {
      case JsSuccess(orderIdList, _) => slickOrders.getBulkOrderDTOs(orderIdList).map { bulkOrderDTOs =>
        Ok(Json.toJson(bulkOrderDTOs))
      }
      case JsError(errors) =>
        Logger.error(s"ERROR VALIDATING LIST : $errors")
        Future.successful(BadRequest(s"Provide correct $errors"))
    }

  }

  def getOrderDtoByOrderItemId(orderItemId: Long): Action[AnyContent] = Action.async { implicit request =>


    slickOrders.getOrderDTOByOrderItemId(OrderItemId(orderItemId)).map { orderDto => Ok(Json.toJson(orderDto)) }.recover {
      case exception =>
        Logger.error(s"Failed returning Future in controller:[getLast15DaysOrders] $exception")
        InternalServerError(s"Failed While Executing in Future $exception")
    }
  }

  /*
  def getOrderItems(): Action[AnyContent] = Action.async { implicit request =>
    slickOrders.getOrderItems().map{orderItems => Ok(Json.toJson((orderItems)))}
  }
  */

  def getAllOrders: Action[AnyContent] = Action { implicit request =>
    val ordersSeq: Seq[(Order, SubOrders, OrderItems)] = Await.result(slickOrders.getAllOrders, 2.seconds)

    println("all Orders Printed", ordersSeq)
    Ok(views.html.viewAllOrders(ordersSeq))
  }
}
//    val ordersSeq: Seq[(Order, SubOrders, OrderItems)] = Await.result( SlickOrders.getAllOrders,2.seconds)

//    println("all Orders Printed")
//    Ok( views.html.ordersPage(ordersSeq) )



  ////
  /*
  def getLast15DaysOrders(userId: Long): Action[AnyContent] = Action.async { implicit request =>

    slickOrders.getAllOrdersLast15Days(UserId(userId)).map( order => Ok(Json.toJson(order)) ).recover{
      case exception =>
        Logger.error(s"Failed returning Future in controller:[getLast15DaysOrders] $exception")
        InternalServerError(s"Failed While Executing in Future $exception")
    }

  }
  */

/*
Input Json [OrderInputDTO]  ----
{
    "userId": 248,
    "userAddressId": 231,
    "orderStatus": "Success",
    "paymentInstrument": "CreditCard",
    "subOrdersList": [
        {
            "sellerId": 908,
            "orderItemsList": [
                {
                    "productId": 2812,
                    "productName": "Bag",
                    "quantity": 3,
                    "price": 200
                },
                {
                    "productId": 1276,
                    "productName": "Ball",
                    "quantity": 2,
                    "price": 123
                }
            ]
        },
        {
            "sellerId": 103,
            "orderItemsList": [
                {
                    "productId": 981,
                    "productName": "Chair",
                    "quantity": 1,
                    "price": 1300
                },
                {
                    "productId": 450,
                    "productName": "Table",
                    "quantity": 5,
                    "price": 4500
                }
            ]
        }
    ]
}
 */
