package controllers

import javax.inject.{Inject, Singleton}
import play.api.mvc.{Action, AnyContent, Controller, Request}
import services.SlickOrdersService
import models._
import scala.concurrent.Await
import scala.concurrent.duration._



@Singleton
class UserLoginController @Inject()(slickOrders: SlickOrdersService)extends Controller(){
  def login() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.login())
  }
  def validate() = Action { implicit request=>
    val postVals = request.body.asFormUrlEncoded

    val validUserMap = Map("umangz" -> "123", "random" -> "random", "guest" -> "guest")
    postVals.map{args =>
      val username = args("username").head
      val password = args("password").head

      val isValid = validUserMap.exists(_ == (username,password))
      if(isValid)
        Ok(views.html.loggedin(username: String))
      else Ok("Either invalid password or user not registered")
    }.getOrElse(Ok("Please enter username and password"))

    //Ok(s"Hello $username. You just logged in with password $password")
  }

  def createOrder: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val productsSeq = Await.result( slickOrders.getAllProducts,2.seconds)

    Ok( views.html.createOrder(productsSeq.toList))
  }


  def enterOrderFromFrom: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
  println("reaching here in enterOrderFromForm")
    val postVals = request.body.asFormUrlEncoded
    postVals.map{ args =>
      println("reaching here")
      val userId = args("userId").head.toLong
      val userAddressId = args("userAddressId").head.toLong
      val paymentInstrument = args("paymentInstrument").head
      val productIds = args("productIds[]").toList

      val productIdsSeq = productIds.map( valu => ProductId(valu.toLong) ).toSeq

      slickOrders.addOrderFromForm(UserId(userId),UserAddressId(userAddressId),PaymentInstrument.toPaymentInstrumentType(paymentInstrument) , productIdsSeq )
      Ok(views.html.orderPlaced())
    }.getOrElse(Ok("Please enter correct Information"))

    //slickOrders.addOrderFromForm()
    //println(postVals)
    /*
    data => {


        val productIdSeq = data._4.map(ProductId(_))
        orderService.addOrderFromFormData(UserId(data._1),UserAddressId(data._2), convertToPaymentInstrumentType(data._3), productIdSeq)
        Ok( views.html.orderForm(data._4.toList))
     */

  }


  /*
  def enterOrder: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val productsSeq = Await.result( slickOrders.getAllProducts,2.seconds)

    Ok( views.html.productsPage(productsSeq.toList))
  }
  */
}
