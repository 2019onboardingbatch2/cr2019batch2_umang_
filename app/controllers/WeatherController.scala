package controllers

import com.google.inject.Inject
import javax.inject._
import play.api.libs.json.JsLookupResult
import play.api.mvc.{Action, Controller}
import services.FetchWeatherInfoService
import services.StoreWeatherInfoServices

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class WeatherController @Inject()(FetchWeatherService: FetchWeatherInfoService, storeWeatherInfoServices: StoreWeatherInfoServices)extends Controller() {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def fetchAndStoreWeather() = Action.async(parse.json) { request =>
    println("prniting something " )
    val requestBody = request.body
    val cityJS : JsLookupResult = requestBody \ "city"
    val cityStr : String = cityJS.as[String]
    val url = s"https://api.apixu.com/v1/current.json?key=65559201bff048fc99883439191208&q=${cityStr}"
    FetchWeatherService.getWeatherData(url).map(strWeatherRes => {
      storeWeatherInfoServices.storeWeatherData(strWeatherRes)
      println(strWeatherRes)
      Ok(strWeatherRes)
    })
  }

  def fetchWeatherData() = Action{
    val cityWeatherData = storeWeatherInfoServices.cityWeatherData
    Ok("cityWeatherData")
  }
}