case class Employee (teamName: String, firstName: String, emplNo: Int) {
  def this(teamName: String, firstName: String, emplNo: Int) = {
    this(teamName, firstName,emplNo)
  }
}

val em = Employee("backend", "John", 346)
println(em)