name := """newplay"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.11"

//libraryDependencies += guice
//libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.example.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.example.binders._"
libraryDependencies += "com.typesafe.play" %% "play-ws" % "2.5.18"

// https://mvnrepository.com/artifact/com.typesafe.slick/slick
//libraryDependencies += "com.typesafe.slick" %% "slick" % "3.2.0"


//libraryDependencies ++= Seq(
//  "com.typesafe.play" %% "play-slick" % "3.2.3",
//  "com.typesafe.play" %% "play-slick-evolutions" % "3.2.3"
//)

//libraryDependencies +="postgresql" % "postgresql" % "9.1-901-1.jdbc4"
libraryDependencies += "org.postgresql" %"postgresql" %"42.2.1"

libraryDependencies ++= Seq("com.typesafe.play" %% "play-slick" % "2.0.0" withSources(),
  "com.typesafe.play" %% "play-slick-evolutions" % "2.0.0" withSources())
libraryDependencies += "org.postgresql" %"postgresql" %"42.2.1" withSources()